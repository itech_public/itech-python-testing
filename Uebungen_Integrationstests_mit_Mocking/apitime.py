# -*- coding: utf-8 -*-
"""A module-level docstring
Dieses Programm ruft eine frei verfügbare "application programming interface" auf (API).
Die API bietet folgendes, siehe auch hier: http://worldtimeapi.org/

WorldTimeAPI is a simple web service which returns the local - time for a given timezone as either JSON or plain-text.
Some additional information is provided, such as whether that timezone is currently in Daylight Savings Time (DST,
when DST starts and ends, the UTC offset, etc.
A full list of timezones can be found here, and further details about this service can be found in the FAQs.

Die oben beschriebene API wird genutzt und diese Modul bietet ein paar Methoden an,
welche die bereitgestellten Daten interpretieren.
"""

import requests
import json

class APITime:

    def get_worldtimeapi_ip(self):
        # Ruft die API worldtimeapi auf
        response = requests.get("http://worldtimeapi.org/api/ip")
        return response.content

    def get_datetime_ip(self):
        # Über get_worldtime_api_ip wird die Datetime für die aktuelle IP ermittelt
        # Beispiel Rückgabewert "2019-11-02T20:39:46.472442+01:00"
        response = self.get_worldtimeapi_ip()
        content = json.loads(response)
        ip_datetime = "For ip {} datetime is: {}".format(content["client_ip"], content["datetime"])
        return ip_datetime

    def get_weeknumber_and_timezone(self):
        # Über get_worldtime_api_ip wird die Datetime für die aktuelle IP ermittelt
        # Beispiel Rückgabewert "2019-11-02T20:39:46.472442+01:00"
        response = self.get_worldtimeapi_ip()
        content = json.loads(response)
        weeknumber_timezone = ip_datetime = "The weeknumber is {}. The timezone is {}.".format(content["week_number"], content["timezone"])
        return weeknumber_timezone

# Verbosity=2 sorgt dafür, dass die Ausgaben "wortreicher" sind.
# Wenn Sie den Level auf 1 ändern, erkennen Sie den Unterschied.
if __name__=='__main__':
    # Beispielaufruf einer Methode der Klasse APITime
    at = APITime()
    response = at.get_worldtimeapi_ip()
    print(response)


